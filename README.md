# $ cat /dev/random > /dev/null 2>&1

Archivos fuente de mi cápsula gemini [«$ cat /dev/random > /dev/null 2>&1»][gemsite].

## Descripción

El sitio es gestionado por medio de [kiln][] y auspiciado por [tilde.team][].

## Contacto

Si desea contactar a su autor, por favor refiérase a: [ivanruvalcaba\[at\]disroot\[dot\]org][email-contacto].

## Licencia

Copyright (c) 2023 — Iván Ruvalcaba. *A no ser que se indique explícitamente lo contrario, el contenido de este sitio se encuentra sujeto a los términos de la licencia [Creative Commons Atribución/Reconocimiento 4.0 Internacional (CC BY 4.0)][cc-by-4.0].* Licencia dual [FSFAP][GNUAllPermissive]/[LGPLv3+][lgpl-3.0] para el código fuente.

[gemsite]: gemini://tilde.team/~ivanruvalcaba/
[kiln]: https://git.sr.ht/~adnano/kiln
[tilde.team]: https://tilde.team/
[email-contacto]: mailto%3Aivanruvalcaba%40disroot.org%3Fsubject%3D%5Bgemini-devrandomdevnull%5D%20Contacto%3A
[cc-by-4.0]: https://creativecommons.org/licenses/by/4.0/deed
[GNUAllPermissive]: https://www.gnu.org/licenses/license-list.en.html#GNUAllPermissive
[lgpl-3.0]: https://www.gnu.org/licenses/lgpl-3.0.en.html
