# http://humanstxt.org/

# TEAM

  Webmaster & Software Craftsman: Iván Ruvalcaba
  Contact: ivanruvalcaba[at]disroot[dot]com
  From: Naucalpan de Júarez, Estado de México | México

# THANKS

  With ❤ to: My beloved family (Luz María, Jean, Iván) and my Father
  gemini, gssg, openring and rofimoji Developers: Hey guys! Without you this would not be easily possible
  Vim Developers: The best text editor ever!!!
  Free & Open Source Developers: You rules!!!

# TECHNOLOGY COLOPHON

  Last update: 2023/05/07
  Standards: twtxt
  Language: Spanish
  Language Markup: Gemtext and Markdown
  Text editor: Emacs
  Static Site Generator: kiln
  Components: ❤ and 🍵
  Software: GNU Make
