# Twtxt is an open, distributed microblogging platform that
# uses human-readable text files, common transport protocols,
# and free software.
#
# == Metadata ==
#
# nick = ivanruvalcaba
# avatar = https://es.gravatar.com/userimage/126413933/86e98ad477bdf9ba7766e8936d3dee09.png
# description = Algunos pensamientos perturbando la psique de Iván Ruvalcaba
# url = gemini://tilde.team/~ivanruvalcaba/twtxt.txt
# link = Gemlog gemini://tilde.team/~ivanruvalcaba//
# follow = prologic https://twtxt.net/user/prologic/twtxt.txt
# follow = adi https://twtxt.net/user/adi/twtxt.txt
# follow = frogorbits https://www.frogorbits.com/twtxt.txt
# follow = kt84 https://twtxt.net/user/kt84/twtxt.txt
# follow = gbmor https://gbmor.org/twtxt.txt
# follow = ew0k gemini://warmedal.se/~bjorn/twtxt.txt
# follow = eaplmx https://twtxt.net/user/eaplmx/twtxt.txt
# follow = Rob gemini://jsreed5.org/feeds/twtxt.txt
# follow = benk gemini://kwiecien.us/twtxt.txt
# follow = apex gemini://rawtext.club/~apex/twtxt.txt
# follow = movq https://www.uninformativ.de/twtxt.txt
# follow = jdtron gemini://tilde.team/~jdtron/twtxt.txt
# follow = lunchboxhero gemini://sdf.org/lunchboxhero/twtxt.txt
# following = 13
#
# == Content ==
2022-04-29T13:59:17-05:00	Dándole un poco de cariño a rustwtxt 💪🏼… #rust #dev
2022-04-29T14:28:26-05:00	¡Feliz cumpleaños cachorro 🎉…! #awesome16
2022-04-30T14:10:07-05:00	Probando «twet», un cliente para feeds «twtxt.txt» desarrollado en Go 🐹 con soporte para HTTP y gemini… Yay!!! 🎉 => https://github.com/jdtron/twet #golang #twtxt #gemini
2022-05-01T15:20:48-05:00	Sería estupendo que gemini contara con soporte para «anchores», para así poder crear páginas wiki en nuestros «gemsites». ¿A caso alguien ya dio con la solución alternativa a esta situación? 🙃… #gemini #wiki
2022-05-04T08:39:13-05:00	Me parece una excelente noticia el saber que, a partir de la versión 1.13.x, «lagrange» cuente con soporte para el protocolo «spartan». En lo personal aspiro y me encantaría poder publicar exclusivamente para «spartan». Espero que este hecho sea un fuerte impulso para la adopción de dicho protocolo 🙏… Además ¡la pluralidad protocolos es genial! Debería ser la norma y no la excepción 💪🏼 #lagrange #gemini #spartan
2022-05-06T11:18:29-05:00	Me resulta truculento el organizar mis anotaciones en «Zim», dado que estamos mal acostumbrados a tomar apuntes de forma lineal, sin embargo creo que comienzo a pillarle el truquillo 😄️… #zim
2022-05-09T19:25:16-05:00	Me atrae mucho la idea de portar mi «gemsite» a «spartan» ahora que «lagrange» cuenta con soporte para este protocolo… El principal inconveniente es encontrar un servicio de «hosting» 🤔… Siempre podría recurrir a «tilde.team» sin embargo, «twet» aún no cuenta con soporte para «spartan», por lo tanto, me quedaré sin la posibilidad de publicar mi feed «twtxt.txt» 😟. Dilemas, dilemas… #divagando
2023-04-24T14:42:00-06:00	«Todas las ideas poderosas se remontan a un arquetipo.»
2023-04-24T14:56:51-06:00	Recuperando una vez más mi espacio en la Geminisphere, espero que la inquietud perdure… 🤞
2023-04-25T18:41:02-06:00	He tenido dias complicados y después el de hoy… 😫
2023-05-01T20:28:58-06:00	Se siente cierta calma aparente en mi nuevo trabajo, espero que perdure 🙏. Han sido días de complicados a difíciles…
2023-05-01T20:50:09-06:00	Me intriga mucho el saber que SSG usan la mayoría de los «geminautas» 🤔. Sugerencias son bienvenidas…
2023-05-07T14:01:39-06:00	Finalmente, me siento conforme con los resultados obtenidos por un SSG para mi sitio «gemini». Siendo el ganador indiscutible: #kiln 🎉. Honestamente, debo confesar que me resulto poco intuitivo el configurarlo a modo de mis exigencias, por lo que tuve que abusar en demasía del método «prueba y error» 😫. Sin embargo, todo ha quedado del modo que cabria de esperar y ahora esto feliz por ello 😄.
2023-05-14T15:45:12-06:00	He logrado configurar tmux + zsh en Windows terminal. Ha sido bastante satisfactorio, sin embargo, son tareas que consumen mucho mi tiempo dada la dedicación que se le pone a este tipo de acciones.
2023-06-05T21:04:45-06:00	He creado un humilde script para Tampermonkey/Violentmonkey y compañía para embellecer el contenido de https://portal.mozz.us/ utilizando «bamboo.css» y, modestia aparte, ¡¡¡luce genial 😁!!!
