# Filename: Makefile
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
#
# Copyright (C) 2023  Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.
#
# See the FSF All-Permissive License for more details
# <https://spdx.org/licenses/FSFAP.html>.

MARKDOWN_DIR?=md
MARKDOWN_FILE?=$(shell bash -c 'read -p "Nombre del archivo Markdown: " file_name; echo $$file_name')
MARKDOWNLINT_OPTIONS?=--disable MD013
BLOG_DIR?=content/blog
PUBLIC_DIR?=public
REMOTEHOST_NAME?=ivanruvalcaba@tilde.team:public_gemini
TIMESTAMP?=`date +%FT%T%:z`

# Variables utilizadas para colorear los mensajes mostrados en la ayuda.
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
CYAN   := $(shell tput -Txterm setaf 6)
RESET  := $(shell tput -Txterm sgr0)

all: help

## help:
help: ## Mostrar la ayuda.
	@echo ''
	@echo '«$$ cat /dev/random > /dev/null 2>&1» Makefile.'
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)

## build:
build: ## Generar el directorio público.
	@echo "Generar el directorio público…"
	kiln build

## clean:
clean: ## Eliminar el directorio público.
	@echo "Eliminar el directorio «${PUBLIC_DIR}»…"
	rm -rf $(PUBLIC_DIR)

## deploy:
deploy: clean build ## Hospedar el directorio público en el host remoto.
	@echo "Desplegar el directrio «${PUBLIC_DIR}» a «${REMOTEHOST_NAME}»…"
	rsync -rtvzP --delete $(PUBLIC_DIR)/ $(REMOTEHOST_NAME)/

## mdconvert:
mdconvert: ## Convertir el contenido de un archivo Markdown a Gemtext.
	@echo "Convertir archivo Markdown a Gemtext…"
	md2gemini \
		--plain \
		--frontmatter \
		--no-checklist \
		--links 'at-end' \
		--write $(MARKDOWN_DIR)/$(MARKDOWN_FILE) \
		--dir $(BLOG_DIR)

## mdlint:
mdlint: ## Ejecutar el «lintern» a los archivos Markdown en el directorio de contenido.
	# Véase: https://github.com/markdownlint/markdownlint/blob/master/docs/RULES.md
	@echo "Realizar el «linting» de los archivos Markdown ubicados en el directorio «${MARKDOWN_DIR}»…"
	markdownlint $(MARKDOWN_DIR)/ $(MARKDOWNLINT_OPTIONS)

## timestamp:
timestamp: ## Imprimir en pantalla la fecha y hora actual del sistema en un formato RFC 3339.
	@echo "Mostrar la fecha y hora actual del sistema…"
	@echo $(TIMESTAMP)

.PHONY: all build clean deploy help mdconvert mdlint timestamp

# vim: setlocal cc=80 ts=8 sw=8 noet:
