---
title: "Futuros tecnológicos: Una carta a la Smolnet"
date: 2023-05-08T19:19:03-06:00
---
A través del agregador de feeds Cosmos[1] pude acceder al siguiente artículo[2], mismo que hace referencia al título que da origen a esta publicación.

En él se hace referencia a una «carta abierta»[3] sobre el tecno capitalismo e inteligencia artificial (IA). Indudablemente, resulta ser una lectura obligada dada la trascendencia que tiene en los tiempos que nos acontecen.

=> gemini://skyjake.fi/~Cosmos/ 1: Cosmos
=> gemini://arcanesciences.com/gemlog/23-05-04/ 2: The "Technological Futures" Letter
=> gemini://perma.computer/letter 3: Technological Futures: A Letter to the Smolnet